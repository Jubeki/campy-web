@extends('_layouts.master')

@section('body')
<main class="p-8 bg-purple-500">
    <div class="text-white text-3xl mb-4">Die Code+Design Events</div>
    <div class="bg-white rounded p-4">
        @include('_partials.events.header')

        @include('_partials.events.upcoming')

        <div class="mt-8">
            @include('_partials.events.description')
        </div>

        <div class="mt-8">
            @include('_partials.events.interest')
        </div>

        <div class="mt-8">
            @include('_partials.events.images')
        </div>
        
        <div class="mt-8">
            @include('_partials.events.testimonials')
        </div>

        <div class="mt-8 mb-4">
            @include('_partials.events.list-done')
        </div>

    </div>
</main>
@endsection

@section('scripts')
    <script src="https://cdn.plyr.io/2.0.18/plyr.js"></script>
    <script>
        plyr.setup("#plyr-youtube");
    </script>
@endsection

@section('meta')
<link rel="stylesheet" href="/css/plyr.css">
@endsection

@section('title')
Events
@endsection
