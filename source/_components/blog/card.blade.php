<div class="p-1 w-full md:w-1/2 xl:w-1/3">
    <div class="bg-white p-4 rounded h-full">
        <div>
            <img src="{{ $blog->image }}" alt="{{ $blog->caption }}" />
            <div class="text-2xl back mb-1 mt-4">
                {{$blog->caption}}
            </div>
            <div class="text-sm mb-4 back text-gray-600">
                {{ $blog->author }} | {{ date('d-m-Y', $blog->published_at) }}
            </div>
            <p class="text-lg mb-4 leading-normal">{{ $blog->lead }}</p>
        </div>
        <div>
            @component('_components.blog.read_more')
                @slot('url', $blog->getUrl())
            @endcomponent
        </div>
    </div>
</div>