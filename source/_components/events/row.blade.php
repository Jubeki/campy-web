<div class="border-2 rounded p-4 bg-gray-400">
    <div class="text-2xl mb-4 text-gray-900">{{ $title ?? 'Events' }}</div>
    <p>{{ $text ?? '' }}</p>
    <div class="flex flex-wrap mt-3">
        @foreach ($events as $event)
            @component('_components.events.card')
                @slot('event', $event)
                @slot('flex', $flex ?? false)
            @endcomponent
        @endforeach
    </div>
</div>