<img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
  class="lazyload mb-4 {{ $height ?? '' }} {{ $width ?? '' }} {{ $rounded ?? '' }} {{ $maxwidth ?? '' }}"
  data-srcset="{{ $src }}" alt="{{ $alt ?? '' }}" />
