@if($pagination->totalPages > 1)
<div class="flex flex-row border border-gray-400 rounded">
    @component('_components.pagination-link')
        @slot('text', '&laquo;')
        @slot('url', $pagination->previous ? $pagination->first : '')
    @endcomponent
    @component('_components.pagination-link')
        @slot('text', '&lsaquo;')
        @slot('url', $pagination->previous)
    @endcomponent
    @foreach ($pagination->pages as $pageNumber => $path)
        @component('_components.pagination-link')
            @slot('text', $pageNumber)
            @slot('url', $path)
            @slot('active', $pageNumber == $pagination->currentPage)
        @endcomponent
    @endforeach
    @component('_components.pagination-link')
        @slot('text', '&rsaquo;')
        @slot('url', $pagination->next)
    @endcomponent
    @component('_components.pagination-link')
        @slot('text', '&raquo;')
        @slot('url', $pagination->next ? $pagination->last : '')
        @slot('border', 'border-none')
    @endcomponent
</div>
@endif