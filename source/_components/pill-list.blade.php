<div class="text-base leading-loose">
    @foreach ($pills as $pill)
        <div class="m-1 bg-purple-200 px-2 rounded inline-block">{{ $pill }}</div>
    @endforeach
</div>