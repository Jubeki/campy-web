<nav class="md:flex items-center justify-between flex-wrap bg-white p-6">
    <div class="flex items-center justify-center md:justify-start flex-shrink-0 text-black mr-6">
        <a href="/">
            <img src="/img/logo.svg" class="w-64 text-black" alt="Code+Design Logo">
        </a>
    </div>
    <div class="w-full block items-center justify-center flex-grow flex  md:items-center md:w-auto">
        <div class="text-2xl md:flex-grow text-center md:text-left">
            <ul class="list-none p-0 inline-block mt-4 sm:mt-0">
                <li class="inline-block">
                    <a href="/events" class=" text-black md:mt-0 border-b-2 border-purple-500 p-1">Events</a>
                </li>
                <li class="inline-block ml-2">
                    <a href="/magazin" class=" text-black md:mt-0 border-purple-500 border-b-2 p-1">Magazin</a>
                </li>
                <li class="inline-block ml-2">
                    <a href="/community" class=" text-black md:mt-0 border-purple-500 border-b-2 p-1">Community</a>
                </li>
                <li class="inline-block ml-2 mt-6 md:mt-0">
                    <a href="/initiative" class=" text-black border-purple-500 border-b-2 md:mt-0 p-1">Initiative</a>
                </li>
            </ul>
        </div>
        <div class="hidden md:block">
            @component('_components.button')
                @slot('url', '/initiative/spenden/')
                Spenden
            @endcomponent
        </div>
    </div>
</nav>
