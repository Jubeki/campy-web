<div class="block bg-white p-8 mt-8 text-lg">
    <div class="flex flex-wrap mb-4">
        <div class="w-full md:w-1/2 lg:w-1/4 h-auto p-2">
            <div class="mb-2 "><span class="border-b-2 border-purple-500">Code+Design</span></div>
            <ul class="list-none p-0 leading-normal">
                <li class="hover:text-purple-500 text-gray-800"><a href="/initiative" class=" hover:text-purple-500 text-gray-800" class=" hover:text-purple-500 text-gray-800">Initiative</a></li>
                <li class="hover:text-purple-500 text-gray-800"><a href="/initiative/partner" class=" hover:text-purple-500 text-gray-800" class=" hover:text-purple-500 text-gray-800">Partner</a></li>
                <li class="hover:text-purple-500 text-gray-800"><a href="/initiative/spenden" class=" hover:text-purple-500 text-gray-800" class=" hover:text-purple-500 text-gray-800">Spenden</a></li>
                <li class="hover:text-purple-500 text-gray-800"><a href="/blog" class=" hover:text-purple-500 text-gray-800" class=" hover:text-purple-500 text-gray-800">Blog</a></li>
                <li class="hover:text-purple-500 text-gray-800"><a href="/initiative/stimmen" class=" hover:text-purple-500 text-gray-800">Stimmen</a></li>
                <li class="hover:text-purple-400 text-gray-800"><a href="/initiative/philosophie" class="text-gray-800  hover:text-purple-500">Philosophie</a></li>
                <li class="hover:text-purple-500 text-gray-800"><a href="/kontakt" class=" hover:text-purple-500 text-gray-800">Kontakt</a></li>
                <li class="hover:text-purple-500 text-gray-800"><a href="/impressum" class=" hover:text-purple-500 text-gray-800">Impressum</a></li>
                <li class="hover:text-purple-500 text-gray-800"><a href="/datenschutz" class=" hover:text-purple-500 text-gray-800">Datenschutz</a></li>
            </ul>
        </div>
        <div class="w-full md:w-1/2 lg:w-1/4 h-auto p-2">
            <div class="mb-2"><span class="border-b-2 border-purple-500 text-gray-900">Events</span></div>

            <ul class="list-none p-0 leading-normal">
                @forelse ($events->where('active', 'yes') as $event)
                    <li class="hover:text-purple-500 text-gray-800"><a href="{{  $event->getUrl() }}" class=" hover:text-purple-500 text-gray-800">{{  $event->city }}, @if ($event->days > 1){{ date('d.m.y', $event->date_start) }}-{{ date('d.m.y', $event->date_end) }}@else {{ date('d.m.y', $event->date_start) }} @endif </a></li>
                @empty
                    <li class="text-gray-800">Keine aktiven Events</li>
                @endforelse

                <li class="hover:text-purple-400 text-gray-800 mt-4"><a href="/events/#past_events" class=" hover:text-purple-500 text-gray-800">Alle vergangenen Events</a></li>

                <li class="hover:text-purple-500 text-gray-800"><a href="/teilnahmebedingungen" class=" hover:text-purple-500 text-gray-800">Teilnahmebedingungen</a></li>

            </ul>
        </div>

            <div class="w-full md:w-1/2 lg:w-1/4 h-auto p-2">
                <div class="mb-2">
                    <a href="/magazin" class="text-gray-900  border-b-2 border-purple-500">Magazin</a>
                </div>
                <ul class="list-none p-0 leading-normal">
                    <li class="text-gray-800"><a href="/files/code-design-magazine-002.pdf" target="_blank" class="text-gray-800  hover:text-purple-600"><svg xmlns="http://www.w3.org/2000/svg" class="inline stroke-current w-4" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-download"><path d="M21 15v4a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2v-4"></path><polyline points="7 10 12 15 17 10"></polyline><line x1="12" y1="15" x2="12" y2="3"></line></svg> Download</a></li>
                    <li class="hover:text-purple-500 text-gray-800"><a href="/magazin/bestellen" class=" hover:text-purple-600 text-gray-800">Bestellen</a></li>
                </ul>
            <div class="mt-6">
                <a href="#" class="text-gray-900  border-b-2 border-purple-500">Unterstützen</a>
                <ul class="list-none p-0 leading-normal mt-2">
                    <li class="text-gray-800"><a href="/coach" class=" hover:text-purple-600 text-gray-800">Coach auf Event</a></li>
                    <li class="text-gray-800"><a href="/initiative/spenden" class=" hover:text-purple-600 text-gray-800">Spenden</a></li>
                    <li class="text-gray-800"><a href="/kontakt" class=" hover:text-purple-600 text-gray-800">Ehrenamtlich tätig werden</a></li>
                </ul>
            </div>
        </div>
        <div class="w-full md:w-1/2 lg:w-1/4 p-2 h-auto">
            <div>
                <div class="mb-2"><span class="border-b-2 border-purple-500">Newsletter</span></div>
                <div>
                    @include('_partials.newsletter')<br>
                </div>
            </div>
            <div class="mt-4 flex" id="social">
                <a href="https://instagram.com/codeunddesign/" target="_blank" class="mr-2"><svg class="stroke-current text-gray-400 hover:text-purple-500 w-8 h-auto" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-instagram"><rect x="2" y="2" width="20" height="20" rx="5" ry="5"></rect><path d="M16 11.37A4 4 0 1 1 12.63 8 4 4 0 0 1 16 11.37z"></path><line x1="17.5" y1="6.5" x2="17.5" y2="6.5"></line></svg></a>

                <a href="https://www.youtube.com/channel/UCuT3xJjPZFqQEEpleHBxVuA/videos" target="_blank"><svg class="stroke-current text-gray-400 hover:text-purple-500 w-8 h-auto" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-play-circle"><circle cx="12" cy="12" r="10"></circle><polygon points="10 8 16 12 10 16 10 8"></polygon></svg></a>

                <a href="https://www.facebook.com/codeunddesign/" target="_blank"><svg class="stroke-current text-gray-400 hover:text-purple-500 w-8 h-auto" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-facebook"><path d="M18 2h-3a5 5 0 0 0-5 5v3H7v4h3v8h4v-8h3l1-4h-4V7a1 1 0 0 1 1-1h3z"></path></svg></a>
            </div>
        </div>

    </div>
</div>
<div class="bg-gray-100 text-gray-600 p-8 leading-normal text-center">
    <p>Die Code+Design Initiative e.V. ist ein gemeinnütziger Verein zur Berufs- und Studienorientierung im Bereich Informatik und Design.</p>
    <p>Vereinsregister VR 35667 B | Code+Design Initiative e.V., Lohmühlenstr. 65, 12435 Berlin</p>
</div>
