<div class="mt-4">
    <div class="text-2xl mb-2">{{ $title ?? 'Team'}}</div>
    <div class="flex flex-wrap">
        @foreach ($team->where('type', $type) as $manager)
        <div class="w-full sm:w-1/2 md:w-1/3 lg:w-1/4 xl:w-1/5 p-1">
            <div class="flex-1 h-full bg-gray-200 px-4 py-8  rounded">
                <div class="w-full h-full">
                    <div class="flex flex-row items-center justify-center p-4">
                        @component('_components.img')
                            @slot('src', $manager->image)
                            @slot('rounded', 'rounded-full')
                            @slot('alt', $manager->firstname . " " . $manager->lastname)
                        @endcomponent
                    </div>
                    <div class="text-center">
                        <p class="text-xl mb-2">{{ $manager->firstname }} {{ $manager->lastname }} </p>
                        @if(!is_null($manager->responsibility))
                            <p class="hyphens"> {{ $manager->responsibility }}</p>
                        @endif
                        @if(!is_null($manager->mail))
                            <a href="mailto:{{ $manager->mail }}" class=" mt-8 text-xs tracking-wider uppercase text-gray-600 hover:text-gray-800">{{ $manager->mail }}</a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>