@component('_components.events.row')
    @slot('title', 'Nächste Events')
    @slot('text', 'Hier stehen alle Events, die schon fix sind und für die du dich anmelden kannst.')
    @slot('events', $events->where('active', 'yes')->push($events->firstWhere('id', '1908-dor')))
    @slot('flex', true)
@endcomponent
