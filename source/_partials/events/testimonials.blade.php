<div class="mb-4 mt-4">
    <div class="text-3xl mb-4 mt-8 ml-4">
        <a href="/initiative/stimmen" class=" border-b-2 border-purple-500 text-gray-900">Die Stimmen</a>
    </div>
    <div class="flex flex-wrap items-stretch">
      @foreach ($testimonials->take(3) as $testimonial)
            <div class="w-full md:w-1/3 p-1">
                @component('_components.card')
                    @slot('src', $testimonial->src)
                    @slot('url', $testimonial->resource)
                    @slot('medium', $testimonial->medium)
                    @slot('channel', $testimonial->channel)
                    @slot('published_at', date('d.m.y', $testimonial->published_at))
                    {{ $testimonial->content }}
                @endcomponent
            </div>
      @endforeach
    </div>
</div>
