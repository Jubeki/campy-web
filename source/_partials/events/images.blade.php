<div class="flex flex-wrap">
        <div class="w-full md:w-1/3">
            @component('_components.img')
                @slot('src', '/img/events/002.jpg')
                @slot('alt', 'Jugendliche schauen auf Monitor')
            @endcomponent
        </div>
        <div class="w-full md:w-1/3">
            @component('_components.img')
                @slot('src', '/img/events/004.jpg')
                @slot('alt', 'Jugendliche begutachten Raspberry Pi')
            @endcomponent
        </div>
        <div class="w-full md:w-1/3">
            @component('_components.img')
                @slot('src', '/img/events/005.jpg')
                @slot('alt', 'Mädchen arbeiten am Computer')
            @endcomponent
        </div>
    </div>