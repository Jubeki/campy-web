<div class="mt-4 mb-4 p-4 bg-purple-100 rounded-lg">
    {{$register_text ?? ""}} <br>
    <a href="{{ $register_url ?? 'https://app.code.design/register' }}" class="bg-purple-500 text-white  inline-block text-3xl font-bold px-4 py-2 leading-none rounded-lg hover:border-transparent hover:bg-purple-600 mt-4" target="_blank">
        Anmelden
    </a>
    <br>
</div>
