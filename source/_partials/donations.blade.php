<div class="border-2 rounded p-4 bg-gray-400 flex flex-col sm:flex-row items-center">

    @component('_components.button')
        @slot('url', '/initiative/spenden')
        Spenden
    @endcomponent

    <p class="mt-2 sm:mt-0 text-center sm:text-left sm:ml-4">
        Willst du uns helfen, Jugendliche für digitale Berufe zu begeistern und zu qualifizeren?
        Eine Spende hilft uns, wir können als gemeinnütziger Verein Spendenbescheinigungen ausstellen.
    </p>

</div>