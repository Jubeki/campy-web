---
pagination:
  collection: magazines
  perPage: 3
---


@extends('_layouts.master')

@section('body')
<main class="p-8 bg-purple-500">
    <div class="text-3xl mb-4 text-white">Das Code+Design Magazin</div>
    <div class="p-8 rounded bg-white">
        <p class="text-xl">Mit dem Code+Design Magazin sollen sich junge Menschen einen Überblick darüber verschaffen können, wie sich das Berufsleben durch die Digitalisierung verändert, welche Berufsbilder dabei entstehen und welche Zukunftschancen Berufe in der digitalen Wirtschaft bieten.</p>
    </div>
    
    <div class="mt-4 inline-block">
        @component('_components.pagination')
            @slot('pagination', $pagination)
        @endcomponent
    </div>

    <div class="mt-4 -mx-1 flex flex-wrap">
        @foreach($pagination->items as $magazin)
            @component('_components.magazin.card')
                @slot('magazin', $magazin)
            @endcomponent
        @endforeach
    </div>
</main>
@endsection

@section('title')
Magazin
@endsection
