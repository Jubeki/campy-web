@extends('_layouts.master')

@section('body')
<div class="p-4 bg-purple-500">
    <div class="bg-white p-4">
        <p class="text-2xl mb-4">Filme</p>
        <p>Welche Hacker-Filme fallen dir noch ein? </p>
        <p>Welche Serien mit Tech-Thematik gefallen dir?</p>
        <p><a href="mailto:hello@code.design?subject=Hacker-Filme">Schreib uns</a> und wir werden drüber schreiben.</p>
    </div>
</div>
@endsection

@section('title')
Filme
@endsection
