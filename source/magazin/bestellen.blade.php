@extends('_layouts.master')

@section('body')
<main class="p-8 bg-purple-500">

    <div class="text-3xl mb-4 text-white">Das Code+Design Magazin</div>

    <div class="p-8 rounded bg-white">
        <div class="text-xl">Wir haben leider keine Restexemplare, die komplette Auflage unseres Magazins ist bereits verschickt.</div>
        <p class="mt-4">Jedoch kannst du es <a class=" text-purple-500 hover:text-purple-600" href="/files/code-design-magazine-002.pdf">hier</a> herunterladen.</p>
   </div>
</main>

@endsection

@section('title')
Magazin
@endsection
