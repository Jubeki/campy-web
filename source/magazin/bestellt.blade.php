@extends('_layouts.master')

@section('body')
<main class="p-8 bg-purple-500">

    <div class="bg-white rounded p-8">
        <div class="text-3xl">
            Du kannst gar nicht bestellt haben, da wir keine Magazine mehr haben.
        </div>
    </div>

</main>

@endsection

@section('title')
Magazin
@endsection
