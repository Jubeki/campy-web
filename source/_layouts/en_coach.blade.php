@extends('_layouts.master')

@section('body')
<div class="p-4 bg-purple-500">
    <div class="text-white text-3xl mb-4">{{ $page->title }}</div>

    <div class="bg-white p-4">
        <div class="flex-1 lg:ml-4 markdown">
            @yield('content')
        </div>
        <!-- <div class="lg:flex">

        </div> -->
    </div>
    <style>
        .markdown h3 {
            margin-top: 1rem;
        }
    </style>
</div>
@endsection
