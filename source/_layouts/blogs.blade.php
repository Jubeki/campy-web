@extends('_layouts.master')

@section('body')

<main class="p-8 bg-purple-500">
    <div class="mx-auto max-w-xl">

        <h1 class="text-white flex flex-col font-normal text-3xl">
            {{ $page->caption }}
            <span class="text-base">{{ $page->author }} | {{ date('d.m.Y', $page->published_at) }}</span>
        </h1>
        <div class="bg-white p-4 rounded leading-normal text-lg mt-4 text-justify">
            <div>
                <div class="float-right w-full md:w-1/2 md:pl-4 pb-1">
                    @if($page->youtube <> '')
                        <div id="plyr-youtube" data-type="youtube" data-video-id="{{$page->youtube}}"></div>
                    @elseif($page->image <> '')
                        <img src="{{$page->image}}" alt="" />
                    @endif
                </div>
                {!! $page->getContent() !!}
            </div>
            <div class="flex flex-col sm:flex-row mt-8">
                <div>
                    @if ($page->getPrevious())
                        @component('_components.button')
                            @slot('url', $page->getPrevious()->getUrl())
                            @slot('width', 'w-full sm:w-auto')
                            &larr; Vorheriger Post
                        @endcomponent
                    @endif
                </div>
                <div class="mt-6 sm:mt-0 sm:ml-auto">
                    @if ($page->getNext())
                        @component('_components.button')
                            @slot('url', $page->getNext()->getUrl())
                            @slot('width', 'w-full sm:w-auto')
                            Nächster Post &rarr;
                        @endcomponent
                    @endif
                </div>
            </div>
        </div>
    </div>
</main>
@endsection

@section('scripts')
<script src="https://cdn.plyr.io/2.0.18/plyr.js"></script>
<script>
    plyr.setup("#plyr-youtube");
</script>
@endsection

@section('meta')
<link rel="stylesheet" href="/css/plyr.css">
@endsection

@section('title')
Blog | {{$page->caption}})
@endsection
