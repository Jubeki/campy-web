@extends('_layouts.master')

@section('body')
<main class="p-8 bg-purple-500">
    <div class="text-3xl mb-4 text-white">Kontakt</div>
    <div class="p-8 rounded bg-white">
        <div id="contact-form"></div>
        <script src="{{ $page->appUrl }}js/contact.js"></script>
        <script>
            window.onload = () => {
                mountContact('#contact-form')
            }
        </script>
    </div>
</main>
@endsection

@section('title')
Kontakt
@endsection
