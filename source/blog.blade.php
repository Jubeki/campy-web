---
pagination:
  collection: blogs
  perPage: 3
---

@extends('_layouts.master')

@section('body')
<main class="p-8 bg-purple-500">
    <div class="text-white text-3xl mb-4 flex flex-row">
        Blog
        <span class="text-sm ml-4">
            @component('_components.pagination', ['pagination' => $pagination])
            @endcomponent
        </span>
    </div>


        <!-- Start: Articles -->
        <div class="flex flex-wrap">
            @foreach($pagination->items as $blog)
                @component('_components.blog.card')
                    @slot('blog', $blog)
                @endcomponent
            @endforeach
        </div>
        <!-- End: Articles -->
</main>
@endsection

@section('scripts')
<script src="https://cdn.plyr.io/2.0.18/plyr.js"></script>
    <script>
        plyr.setup("#plyr-youtube");
    </script>
@endsection

@section('meta')
<link rel="stylesheet" href="/css/plyr.css">
@endsection

@section('title')
Blog
@endsection
