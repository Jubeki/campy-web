---
extends: _layouts.partners
name: Michael Wessel Informationstechnologie GmbH
logo: michaelwessel.jpeg
section: test
tier: bronze
events: han1808
website: https://blog.michael-wessel.de/
instagram:
twitter:
facebook:
short: ''
---