@extends('_layouts.master')

@section('body')
<main class="p-8 bg-purple-500">
<div class="text-white text-3xl mb-4">Die Code+Design Initiative</div>
<div class="bg-white rounded p-4">

    <div>
        <div class="text-2xl">Die Initiative</div>
        <p class="text-lg leading-normal">
            Die gemeinnützige Code+Design Initiative hat sich zum Ziel gesetzt, Jugendliche für digitale Technologien und Berufe zu begeistern
            und insbesondere den Anteil an Frauen in diesen Bereichen zu erhöhen. Dafür führt sie Code+Design Camps in verschiedenen Orten durch
            und gibt ein Magazin mit IT-Themen für Jugendliche heraus.
        </p>
    </div>
    
    @include('_partials.team.section', ['title' => 'Team', 'type' => 'core'])
    @include('_partials.team.section', ['title' => 'Kuratorium', 'type' => 'kuratorium'])
    @include('_partials.team.section', ['title' => 'Ehrenamtliche Helfer', 'type' => 'helper'])

    <div class="mt-4">
        @include('_partials.donations')
    </div>
</div>
</main>
@endsection

@section('title')
Initiative
@endsection
